package ru.t1.amsmirnov.taskmanager.api.repository.model;

import ru.t1.amsmirnov.taskmanager.model.Project;

public interface IProjectRepository extends IAbstractUserOwnedRepository<Project> {
}
