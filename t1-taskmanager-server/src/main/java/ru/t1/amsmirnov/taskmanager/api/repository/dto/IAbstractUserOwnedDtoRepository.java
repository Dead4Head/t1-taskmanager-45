package ru.t1.amsmirnov.taskmanager.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.AbstractUserOwnedModelDTO;

import java.util.Comparator;
import java.util.List;

public interface IAbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends IAbstractDtoRepository<M> {

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAllSorted(@NotNull String userId, @Nullable String sort);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);

}
