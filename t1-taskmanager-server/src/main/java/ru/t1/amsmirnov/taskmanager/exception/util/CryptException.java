package ru.t1.amsmirnov.taskmanager.exception.util;

public final class CryptException extends Exception {

    public CryptException() {
        super("Fatal Error! Contact administrator...");
    }

    public CryptException(final String message) {
        super(message);
    }

    public CryptException(final Throwable cause) {
        super(cause);
    }

}
