package ru.t1.amsmirnov.taskmanager.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IProjectDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IUserDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.repository.dto.ProjectDtoRepository;
import ru.t1.amsmirnov.taskmanager.service.ConnectionService;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class UserRepositoryTest {
    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private SqlSession connection;

    @NotNull
    private IProjectDtoRepository projectRepository;


    @NotNull
    private IUserDtoRepository userRepository;

    @NotNull EntityManager entityManager;

    @NotNull EntityTransaction transaction;

    @NotNull
    private final List<UserDTO> users = new ArrayList<>();

    @Before
    public void initRepository() throws Exception {
        transaction = entityManager.getTransaction();
        projectRepository = new ProjectDtoRepository(entityManager);
        userRepository = connection.getMapper(IUserDtoRepository.class);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final UserDTO user = new UserDTO();
            user.setFirstName("First Name " + i);
            user.setPasswordHash("HASH!!!!");
            user.setLastName("Last Name " + i);
            user.setMiddleName("Middle Name " + i);
            user.setEmail("user" + i + "@tm.ru");
            user.setLogin("USER" + i);
            user.setRole(Role.USUAL);
            try {
                userRepository.add(user);
                users.add(user);
            } catch (final Exception e) {
                connection.rollback();
                throw e;
            }
        }
    }

    @After
    public void clearRepository() throws Exception {
        try {
            userRepository.removeAll();
            users.clear();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindByEmail() throws Exception {
        try {
            for (final UserDTO user : users) {
                assertEquals(user, userRepository.findOneByEmail(user.getEmail()));
            }
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindByLogin() throws Exception {
        try {
            for (final UserDTO user : users) {
                assertEquals(user, userRepository.findOneByLogin(user.getLogin()));
            }
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testIsEmailExist() throws Exception {
        try {
            for (final UserDTO user : users) {
                assertTrue(userRepository.isEmailExist(user.getEmail()));
                assertFalse(userRepository.isEmailExist(""));
            }
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testIsLoginExist() throws Exception {
        try {
            for (final UserDTO user : users) {
                assertTrue(userRepository.isLoginExist(user.getLogin()));
                assertFalse(userRepository.isEmailExist(user.getLogin() + user.getLogin()));
            }
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        }
    }

}
